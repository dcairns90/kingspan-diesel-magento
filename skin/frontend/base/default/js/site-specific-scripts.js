﻿//Fancybox
$(document).ready(function () {
    $(".fancybox").fancybox({
        padding: 0,
        arrows: true,
        nextSpeed: 'medium',
        prevSpeed: 'medium'
    });
    $(".fancybox-media").fancybox({
        padding: 0,
        arrows: true,
        nextSpeed: 'medium',
        prevSpeed: 'medium',
        helpers: {
            media: {},
            buttons: {}
        }
    });
    $(".various").fancybox({
        fitToView: false,
        width: '90%',
        height: '90%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        padding: 0,
        closeEffect: 'none',
        iframe: { preload: false }
    });
});

//Homepage Menu
ddaccordion.init({
    headerclass: "mobile-navigation-trigger",
    contentclass: "mobile-navigation",
    revealtype: "click",
    mouseoverdelay: 200,
    collapseprev: true,
    defaultexpanded: [],
    onemustopen: false,
    animatedefault: false,
    scrolltoheader: false,
    persiststate: false,
    toggleclass: ["", ""],
    togglehtml: ["none", "", ""],
    animatespeed: "fast",
    oninit: function (expandedindices) { },
    onopenclose: function (header, index, state, isuseractivated) { }
})

//Equal height columns
equalheight = function (container) {
    var currentTallest = 0,
         currentRowStart = 0,
         rowDivs = new Array(),
         $el,
         topPosition = 0;
    $(container).each(function () {
        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;
        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}
$(window).load(function () {
    equalheight('footer .equal-height');
    equalheight('.owl-carousel .owl-item .item h2');
    equalheight('.product h4');
    equalheight('.category h2 span');
    equalheight('.checkbox-outer');
});
$(window).resize(function () {
    equalheight('footer .equal-height');
    equalheight('.owl-carousel .owl-item .item h2');
    equalheight('.product h4');
    equalheight('.category h2 span');
    equalheight('.checkbox-outer');
});

//Burger Nav overlay
$(document).ready(function () {
    $(".hamburger").on('click', function () {
        $(".hamburger, .hamburger-navigation").toggleClass('is-active');
    });
});

$(document).ready(function () {
    $(".radio-outer label.ui-state-focus").click(function () {
        $(this).closest('div').toggleClass('is-active');
        $(this).closest('div').siblings().removeClass('is-active');
    });
    $(".radio-outer input").click(function () {
        $(this).closest('div').toggleClass('is-active');
        $(this).closest('div').siblings().removeClass('is-active');
    });
});

$(document).ready(function () {
    $(".checkbox-outer label.ui-state-active").parent('div').addClass('is-active');
});

$(document).ready(function () {
    $('.checkbox-outer label').on('click', function (e) {
        $(this).parent('div').addClass('is-active');
    });
    $('.checkbox-outer label').on('click', function (e) {
        if ($(this).hasClass('ui-state-active')) {
            $(this).parent('div').removeClass('is-active');
        }
    });
    $('.checkbox-outer input').on('click', function (e) {
        $(this).parent('div').addClass('is-active');
    });
    $('.checkbox-outer input').on('click', function (e) {
        if ($(this).siblings().hasClass('ui-state-active')) {
            $(this).parent('div').removeClass('is-active');
        }
    });
});