﻿//Fancybox

var j = jQuery.noConflict();


j(document).ready(function () {

    j(".fancybox").fancybox({

        padding: 0,

        arrows: true,

        nextSpeed: 'medium',

        prevSpeed: 'medium'

    });

    j(".fancybox-media").fancybox({

        padding: 0,

        arrows: true,

        nextSpeed: 'medium',

        prevSpeed: 'medium',

        helpers: {

            media: {},

            buttons: {}

        }

    });

    j(".various").fancybox({

        fitToView: false,

        width: '90%',

        height: '90%',

        autoSize: false,

        closeClick: false,

        openEffect: 'none',

        padding: 0,

        closeEffect: 'none',

        iframe: { preload: false }

    });

});



//Homepage Menu

ddaccordion.init({

    headerclass: "mobile-navigation-trigger",

    contentclass: "mobile-navigation",

    revealtype: "click",

    mouseoverdelay: 200,

    collapseprev: true,

    defaultexpanded: [],

    onemustopen: false,

    animatedefault: false,

    scrolltoheader: false,

    persiststate: false,

    toggleclass: ["", ""],

    togglehtml: ["none", "", ""],

    animatespeed: "fast",

    oninit: function (expandedindices) { },

    onopenclose: function (header, index, state, isuseractivated) { }

})



//Equal height columns

equalheight = function (container) {

    var currentTallest = 0,

         currentRowStart = 0,

         rowDivs = new Array(),

         jel,

         topPosition = 0;

    j(container).each(function () {

        jel = j(this);

        j(jel).height('auto')

        topPostion = jel.position().top;

        if (currentRowStart != topPostion) {

            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {

                rowDivs[currentDiv].height(currentTallest);

            }

            rowDivs.length = 0; // empty the array

            currentRowStart = topPostion;

            currentTallest = jel.height();

            rowDivs.push(jel);

        } else {

            rowDivs.push(jel);

            currentTallest = (currentTallest < jel.height()) ? (jel.height()) : (currentTallest);

        }

        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {

            rowDivs[currentDiv].height(currentTallest);

        }

    });

}

j(window).load(function () {

    equalheight('footer .equal-height');

    equalheight('.owl-carousel .owl-item .item h2');

    equalheight('.product h4');

    equalheight('.category h2 span');

    equalheight('.checkbox-outer');

});

j(window).resize(function () {

    equalheight('footer .equal-height');

    equalheight('.owl-carousel .owl-item .item h2');

    equalheight('.product h4');

    equalheight('.category h2 span');

    equalheight('.checkbox-outer');

});



//Burger Nav overlay

j(document).ready(function () {

    j(".hamburger").on('click', function () {

        j(".hamburger, .hamburger-navigation").toggleClass('is-active');

    });

});



j(document).ready(function () {

    j(".radio-outer label.ui-state-focus").click(function () {

        j(this).closest('div').toggleClass('is-active');

        j(this).closest('div').siblings().removeClass('is-active');

    });

    j(".radio-outer input").click(function () {

        j(this).closest('div').toggleClass('is-active');

        j(this).closest('div').siblings().removeClass('is-active');

    });

});



j(document).ready(function () {

    j(".checkbox-outer label.ui-state-active").parent('div').addClass('is-active');

});



j(document).ready(function () {

    j('.checkbox-outer label').on('click', function (e) {

        j(this).parent('div').addClass('is-active');

    });

    j('.checkbox-outer label').on('click', function (e) {

        if (j(this).hasClass('ui-state-active')) {

            j(this).parent('div').removeClass('is-active');

        }

    });

    j('.checkbox-outer input').on('click', function (e) {

        j(this).parent('div').addClass('is-active');

    });

    j('.checkbox-outer input').on('click', function (e) {

        if (j(this).siblings().hasClass('ui-state-active')) {

            j(this).parent('div').removeClass('is-active');

        }

    });

});