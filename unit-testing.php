<?php
require_once('app/Mage.php'); 
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$_productCollection = Mage::getResourceModel('catalog/product_collection');
foreach ($_productCollection as $product) 
{

$description = $product->getDescription();
$strip = strip_tags($description);

$product->setDescription($strip);


$summary = $product->getShortDescription;
$stripsummary = strip_tags($summary);

$product->setShortDescription($stripsummary);
$product->save();

echo "Item " . $product->getSku() . " updated " . PHP_EOL;         
}


?>
