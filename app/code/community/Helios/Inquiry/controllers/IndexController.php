<?php

/**
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Helios_Inquiry_IndexController extends Mage_Core_Controller_Front_Action {

    public function getOptionsWithValues($item) {
        if($item->getOptionByCode('attributes')) {
			$attributes = $item->getOptionByCode('attributes')->getValue();
			if (!$attributes) {
				return null;
			}
		}
        

        $attributes = unserialize($attributes);
        $options = array();

        foreach ($attributes as $attr_id => $attr_value) {
            $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attr_id);

            $attr_options = $attribute->getSource()->getAllOptions(false);

            foreach ($attr_options as $option) {
                if ($option['value'] == $attr_value) {
                    $options[$attribute->getFrontendLabel()] = $option['label'];
                }
            }
        }
        $options = array_unique($options);
        return $options;
    }

    public function indexAction() {
        $this->loadLayout(array('default'));
        $this->_initLayoutMessages('core/session');
        $this->renderLayout();

        if ($_POST['SUBMIT'] == 'SUBMIT') {
            $produkteinfo = $this->getRequest()->getParam("produkteinfo");
            $produckteinfos = unserialize(base64_decode($produkteinfo));

            $fname = $this->getRequest()->getParam("firstname");
            $lname = $this->getRequest()->getParam("lastname");
            $company = $this->getRequest()->getParam("company");

            //$subject =  $this->getRequest()->getParam("subject");
            $subject = "Test";
            $road = $this->getRequest()->getParam("city") . "," . $this->getRequest()->getParam("state");

            $zip = $this->getRequest()->getParam("zip");
            $location = $this->getRequest()->getParam("country");

            $phone = $this->getRequest()->getParam("phone");
            $email = $this->getRequest()->getParam("email");
            $bdesc = addslashes($this->getRequest()->getParam("comments"));
            $headers = "";

            $requiresoldtanklifted = $this->getRequest()->getParam("requiresoldtanklifted");
            $furtherinfo = $this->getRequest()->getParam("furtherinfo");

            $insertArr = array("produckteinfo" => $produkteinfo, "firstname" => $fname, "lastname" => $lname, "company" => $company, "road" => $road, "zip" => $zip, "location" => $location, "phone" => $phone, "email" => $email, "desc" => $bdesc, "iscustcreated" => 0, "status" => 1, "createddt" => date('Y-m-d H:i:s'));
            $collection = Mage::getModel("inquiry/inquiry");
            $collection->setData($insertArr); //
            $collection->save();


            $adminContent = '<table border="1">
							<tr>
								<td>
									<table border="0">
										<tr>
											<Td><label>Hello Administrator,</label></Td>
										</tr>
										<tr>
											<Td><p>Mr/Ms. ' . $fname . ' ' . $lname . ' have requested a product and details are below.</p></td>
										</tr>
										<tr>
												<td>
													<table border="0">
														<tr>
													<td><label>First Name:</label></td>
													<td><label>' . $fname . '</label></td>
												</tr>
												<tr>
													<td><label>Last Name:</label></td>
													<td><label>' . $lname . '</label></td>
												</tr>
												<tr>
													<td><label>Company:</label></td>
													<td><label>' . $company . '</label></td>
												</tr>
												<tr>
													<td><label>Subject:</label></td>
													<td><label>' . $subject . '</label></td>
												</tr>
												<tr>
													<td><label>Road:</label></td>
													<td><label>' . $road . '</label></td>
												</tr>

												<tr>
													<td><label>Postal Code:</label></td>
													<td><label>' . $zip . '</label></td>
												</tr>
												<tr>
													<td><label>Location:</label></td>
													<td><label>' . $location . '</label></td>
												</tr>

											
												<tr>
													<td><label>Contact Phone Number:</label></td>
													<td><label>' . $phone . '</label></td>
												</tr>
												<tr>
													<td><label>Email:</label></td>
													<td><label>' . $email . '</label></td>
												</tr>
												
												<tr>
													<td valign="top" width="15%"><label>Description:</label></td>
													<td><label>' . $bdesc . '</label></td>
												</tr>
												 

												<tr>
													<td valign="top" width="15%"><label>Customer requires their old tank lifted (yes/no):</label></td>
													<td><label>' . $requiresoldtanklifted . '</label></td>
												</tr>
													<tr>
													<td valign="top" width="15%"><label>Customer is interested in further information on the Titan Tank Recycling Scheme (yes/no) :</label></td>
													<td><label>' . $furtherinfo . '</label></td>
												</tr>
												<tr><td colspan="2">&nbsp;</td></tr>
												
												</table>
									    	<div class="grid">
											    <div class="hor-scroll">
												<table cellspacing="0" id="cmsPageGrid_table" class="data">
											     <col width="10">
											    <thead>
											      <tbody>
													
													<tr class="even pointer">
											        	<td width="10%"></td>
														<td width="15%"><strong>Product</strong></td>
														<td width="15%"><strong>Model/Part number</strong></td>
														<td width="15%"><strong>Quantity</strong></td>
														
														<td width="15%"><strong>Options</strong></td>
											        </tr>
											        ';
            $i = 0;

            foreach ($produckteinfos as $prodata) {
                $productId = $prodata['product'];
                $product = Mage::getModel('catalog/product')->load($productId);
                //if($product->getImage()== 'no_selection'){
                //    $image = $this->getSkinUrl('images/placeholder/thumbnail.jpg');
                // }else {
                $image = $product->getImageUrl();
                // }
                $_productName = $product->getName();
                $_SKU = $product->getSku();
                $_qty = $prodata['qty'];
                $_description = $product->getShortDescription();


                Mage::log($produckteinfos);



                $adminContent = $adminContent . '
													<tr class="even pointer">
											            <td align="center"><img src="' . $image . '" width="100" height="100" /></td>
														<td align="left">' . $_productName . '</td>
														<td align="left">' . $_SKU . '</td>
														<td align="left">' . $_qty . '</td>';



                $optiondata = "";

                $customer = Mage::getSingleton('customer/session')->getCustomer();
                Mage::log($customer);


                if ($customer->getId()) {
                    $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);

                    $wishListItemCollection = $wishlist->getItemCollection();

                    Mage::log($wishListItemCollection);
                    $adminContent .= '<td align="left">';
                    foreach ($wishListItemCollection as $item) {
                        if ($item->getData('product_id') == $productId) {
                            $options = $this->getOptionsWithValues($item);
                            foreach ($options as $key => $value) {
                                $adminContent .= '<p>' . $key . ': ' . $value . '</p>';
                            }
                        }
//     	$options = $item->getOptionList();
//		if ($options)
//		{
//			foreach ($options as $option)
//			{
//				$optiondata .= $option['value'];
//			}
//		}
                    }
                    $adminContent .= '</td>';
                }

//$adminContent .= '<td align="left">' .$optiondata.  '</td>';
                // $options = $product->getOptio;
                // if ($options){ 
                //  	$_option_details = $product->__('Options Details');
                //  	foreach ($options as $option){ 
                //  		$_option_label =  $product->escapeHtml($option['label']);
                // 	    $_option_value = nl2br(implode("\n", $option['value']));
                // 		$_option_value2 = $option['value'];
                // 		 if (is_array($option['value'])){
                // 	                 '<td align="left">'.$_option_value.'</td>';
                // 	       } 
                // 	     else{ 
                // 	                  '<td align="left">'.$_option_value2.'</td>';
                // 	       }
                // 	 }
                // 	}

                ' </tr><tr></tr>';


                $i++;
            }

            $adminContent = $adminContent . '  </tbody>
												</table>
											</div>
											</div>
											</td>
										</tr>
										<tr>
													<td colspan="2"><label>Thank You.</label></td>
												</tr>
									</table>
								</td>
							</tr>
						</table>';
            $adminSubject = "Product Quotation Enquiry from New Customer";
            $adminName = Mage::getStoreConfig('quotation/emailsetting/sender_email'); //sender name
            $adminEmail = Mage::getStoreConfig('quotation/emailsetting/sender_email');
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= 'From:sales@commercialtanksonline.com';
            ///Comment the mail functionality while using in local in below than ver1.4.1
            //beginning og mail functionality.......................


            mail($adminEmail, $adminSubject, $adminContent, $headers);

            $customerContent = '
<style>
table, tr, td { margin:0; padding:0; border:0;}
html { background: #EAEAEA; }
</style>
<table border="0" cellspacing="0" cellpadding="20" style="border: 1px solid #aaa; border-radius: 10px; background: #fff; margin: 0 auto;">
<tr>
	<td>
		<table border="0" style="width: 1000px; font-family: Arial, sans-serif;">
            <tr height="10"></tr>
			<tr>
            <td width="10"></td>
				<td width="1000px"><img src="' . Mage::getBaseUrl() . '/skin/frontend/twb_package/default/images/logo.png"></td>
			</tr>
		</table>

        <tr height="10"></tr>

		<table cellpadding="20" style="width: 1000px; font-family: Arial, sans-serif; color: #1f1f1f;  background: #333333; color: #fff;">
        
			<tr>
            <td width="10"></td>
				<td>
					<h2><img src="' . Mage::getBaseUrl() . 'images/tick-icon.png" style="width: 25px">&nbsp;&nbsp;Thanks for submitting your quote</h2> 
					<p>A member of our expert DieselPRO team is currently reviewing your tank requirements, and we will be in touch within two business days with a quote for your Kingspan DieselPro tank solution.</p></td>
                    <td width="10"></td>
			</tr>
        <tr height="10" style="background: #333333;"></tr>
		</table>	

		<table cellpadding="20" style="width: 1000px; font-family: Arial, sans-serif; color: #1f1f1f;">
        <tr height="10"></tr>
				<tr>
                <td width="10"></td>
					<td>
						<h3><img src="' . Mage::getBaseUrl() . 'images/quote-icon.png" width="18px" style="width: 18px; vertical-align: -1px;">&nbsp;&nbsp;Your Quote Summary</h3>
						<table cellspacing="0" id="cmsPageGrid_table" class="data">
											     <col width="10">
											    <thead>
											      <tbody>
													
													<tr class="even pointer">
											        	<td width="10%"></td>
														<td width="15%"><strong>Product</strong></td>
														<td width="15%"><strong>Model/Part number</strong></td>
														<td width="15%"><strong>Quantity</strong></td>
														
														<td width="15%"><strong>Options</strong></td>
											        </tr>
											        ';
            $i = 0;

            foreach ($produckteinfos as $prodata) {
                $productId = $prodata['product'];
                $product = Mage::getModel('catalog/product')->load($productId);
                //if($product->getImage()== 'no_selection'){
                //    $image = $this->getSkinUrl('images/placeholder/thumbnail.jpg');
                // }else {
                $image = $product->getImageUrl();
                // }
                $_productName = $product->getName();
                $_SKU = $product->getSku();
                $_qty = $prodata['qty'];
                $_description = $product->getShortDescription();


                Mage::log($produckteinfos);



                $customerContent = $customerContent . '
													<tr class="even pointer">
											            <td align="center"><img src="' . $image . '" width="100" height="100" /></td>
														<td align="left">' . $_productName . '</td>
														<td align="left">' . $_SKU . '</td>
														<td align="left">' . $_qty . '</td>';



                $optiondata = "";

                $customer = Mage::getSingleton('customer/session')->getCustomer();
                Mage::log($customer);


                if ($customer->getId()) {
                    $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);

                    $wishListItemCollection = $wishlist->getItemCollection();

                    Mage::log($wishListItemCollection);
                    $customerContent .= '<td align="left">';
                    foreach ($wishListItemCollection as $item) {
                        if ($item->getData('product_id') == $productId) {
                            $options = $this->getOptionsWithValues($item);
                            foreach ($options as $key => $value) {
                                $customerContent .= '<p>' . $key . ': ' . $value . '</p>';
                            }
                        }

                    }
                    $customerContent .= '</td>';
                }



                ' </tr><tr></tr>';


                $i++;
            }

            $customerContent = $customerContent . '  </tbody>
												</table>
						<p style="font-size:12px;">*Please note, if you would like to amend any of the information above, speak to a member of the DieselPRO team on UK: +44 (0) 28 3826 4606 / IRL: +353 (0) 42 969 0022 or via email at <a href="mailto:info@commercialtanksonline.com">info@commercialtanksonline.com</a>. We are happy to help. </p>
					</td>
                    <td width="10"></td>
				</tr>
		</table>

		<table style="width: 640px; font-family: Arial, sans-serif; color: #1f1f1f; padding: 20px;">
        <tr height="10"></tr>	
			<tr>
            <td width="10"></td>
				<td><h3><img src="' . Mage::getBaseUrl() . '/images/advice-icon.png" style="width: 18px; vertical-align: -1px;">&nbsp;&nbsp;Need advice on your diesel storage solution?</h3>
					<p><strong style="color: #124B8C;">Tel:&nbsp;</strong>UK: +44 (0) 28 3826 4606 / IRL: +353 (0) 42 969 0022</p>
					<p><strong style="color: #124B8C;">Email:</strong> <a href="mailto:info@commercialtanksonline.com">info@commercialtanksonline.com</a></p>
					<p>We&#8217;re on hand with professional advice on the correct choice of steel tank for your site.</p>
                    <p> That&#8217;s exactly what you would expect from DieselPRO, the global experts in diesel storage and dispensing solutions.</p></td>
			</tr>
		</table>

		<table style="width: 640px; font-family: Arial, sans-serif; padding: 20px;">
        <tr height="10"></tr>
				<tr>
                <td width="10"></td>
					<td colspan="2">Thank You from the DieselPRO team.</td>
				</tr>
                <tr height="10"></tr>
			</table>
		</table>
	</td>
</tr>
</table>

            		';
            $headers = "";
            $adminName = Mage::getStoreConfig('quotation/emailsetting/title_email'); //sender name
            $custSubject = "Thank you for contacting " . $adminName . "";
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From:sales@commercialtanksonline.com';
            //end mail functionaliy...............
            mail($email, $custSubject, $customerContent, $headers);

            Mage::getSingleton('core/session')->addSuccess(Mage::helper('inquiry')->__('Your inquiry was submitted and will be responded to as soon as possible.'));
            $this->_redirect('inquiry/index/thanks');
        }
    }

    public function delAction() {
        $getUrl = Mage::getSingleton('adminhtml/url')->getSecretKey("adminhtml_mycontroller", "delAction");

        $delid = $this->getRequest()->getParam('delid');
        if (!empty($delid)) {
            $collection = Mage::getModel("inquiry/inquiry")->load($delid);

            if ($collection->delete()) {
                Mage::getSingleton('core/session')->addSuccess("Inquire deleted successfully.");
            } else {
                Mage::getSingleton('core/session')->addError("Sorry inquire is not deleted.");
            }
        }

        $this->_redirectReferer();
    }

    public function thanksAction() {
        $this->loadLayout(array('default'));
        $this->renderLayout();
    }

}
